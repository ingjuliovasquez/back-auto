<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auto extends Model
{
    //
    public $table = 'autos';
    public $fillable = [
        'id',
        'marca',
        'modelo',
        'tipo',

    ];
}
