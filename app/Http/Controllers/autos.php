<?php

namespace App\Http\Controllers;
use App\Auto;
use Illuminate\Http\Request;

class autos extends Controller
{
    //

    function index(){

        $auto = Auto::all();
        return $auto;
    }


    function store(Request $request){

        $auto = Auto::create([

            'marca' => $request->marca,
            'modelo' => $request->modelo,
            'tipo' => $request->tipo,

        ]);

         return response()->json($auto);


    }

    function update(Request $request){

        $id= $request->id;
        $auto = Auto::find($id);
        $auto->marca  = $request->marca;
        $auto->modelo  = $request->modelo;
        $auto->tipo = $request->tipo;
        $auto->save();
         return response()->json($auto);


    }


    function delete(Request $request){

      $id= $request->id;
      Auto::find($id)->delete();
    return response()->json(true);


    }
}
