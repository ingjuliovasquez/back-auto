<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('locations')->insert([
            ['id' => 1, 'nombre'  => 'SKU01',  'descripcion'  => 'descripcion1' ,  'municipio'  => 'Xalapa'],
            ['id' => 2, 'nombre'  => 'SKU02',  'descripcion'  => 'descripcion2', 'municipio'  => 'Xalapa'],
            ['id' => 3, 'nombre'  => 'SKU03',  'descripcion'  => 'descripcion3','municipio'  => 'Xalapa'],
            ['id' => 4, 'nombre'  => 'SKU04',  'descripcion'  => 'descripcion4', 'municipio'  => 'Xalapa']
        ]);


    }
}
